defmodule CuriousMessengerWeb.CountryLive do
  use CuriousMessengerWeb, :live_view
  use Phoenix.HTML


  require Logger

  @countries Countries.all() |> Enum.sort_by(&(&1.name))

  def render(assigns) do
    ~H"""
      <div
        phx-hook="SelectCountry"
        phx-update="ignore"
        id="countrySelect2"
      >
        <select name="country">
          <option value="">None</option>
          <%= for c <- @countries do %>
            <option value={c.alpha2}>
              <%= c.name %>
            </option>
          <% end %>
        </select>
      </div>

      <%= if @country do %>
        <!-- Google Maps iframe -->
        <iframe src={"http://maps.google.com/maps?q=" <> @country.name <> "&output=embed"}
          width="360" height="270" frameborder="0" style="border:0"></iframe>

      <% end %>
    """
  end

  def mount(_params, _session, socket) do
    socket =
      socket
      |> assign(:countries, @countries)
      |> assign(:country, nil)

    {:ok, socket}
  end

  def handle_event("country_selected", %{"country" => code}, socket) do
    country = Countries.filter_by(:alpha2, code) |> List.first()
    {:noreply, assign(socket, :country, country)}
  end
end
